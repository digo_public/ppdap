"Digo Http Client."

import json
import logging

import requests

from .const import HTTP_TIMEOUT

_LOGGER = logging.getLogger(__name__)


class DigoHttpClient:
    """Class representing DigoHttpClient."""

    def __init__(
        self,
        host: str,
        port: int,
        tenant_id: str,
        token: str,
    ) -> None:
        """Initialize the DigoHttpClient class."""
        self._http_host = host
        self._http_port = port
        self._tenant = tenant_id
        self._token = token

    @property
    def http_host(self) -> str:
        """Return the host."""
        return self._http_host

    @property
    def http_port(self) -> int:
        """Return the port."""
        return self._http_port

    def post(self, path: str, data: dict, headers: dict | None = None) -> dict:
        """Post data to Digo."""
        if self._http_port == -1:
            url = f"https://{self._http_host}{path}"
        else:
            url = f"http://{self._http_host}:{self._http_port}{path}"
        if not headers:
            headers = {
                "Identifier": self._tenant,
                "Content-Type": "application/json",
                "Authorization": f"Bearer {self._token}",
            }
        try:
            response = requests.post(
                url, json=data, headers=headers, timeout=HTTP_TIMEOUT
            )
            _LOGGER.debug("Http post to %s: %s", path, json.dumps(data))
            return response.json() if response.status_code == 200 else None
        except requests.exceptions.RequestException as e:
            _LOGGER.error("Http post exception: %s", str(e))
            return {}

    def get(
        self, path: str, param: dict | None = None, headers: dict | None = None
    ) -> dict:
        """Get data from Digo."""
        if self._http_port == -1:
            url = f"https://{self._http_host}{path}"
        else:
            url = f"http://{self._http_host}:{self._http_port}{path}"
        if not headers:
            headers = {
                "Identifier": self._tenant,
                "Content-Type": "application/json",
                "Authorization": f"Bearer {self._token}",
            }

        try:
            response = requests.get(
                url, params=param, headers=headers, timeout=HTTP_TIMEOUT
            )
            return response.json() if response.status_code == 200 else None
        except requests.exceptions.RequestException as e:
            _LOGGER.error("Http get exception: %s", str(e))
            return {}
