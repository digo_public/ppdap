"""Define the DigoDevice class."""  # noqa: INP001

import asyncio
from collections.abc import Callable
import json
import logging
import os
from typing import Any

from .attribute import DigoAttribute
from .connection import DigoConnection
from .const import MQTT_CONNECTED
from .mqttc import ReceiveMessage
from .schedule import DigoSchedule
from .setup import DigoDeviceSetup
from .storage import DigoStore
from .utils import callback, generate_transcode, get_timestamp
import sys

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class DigoDevice:
    """Represents a DigoDevice object for communication with a Digo device."""

    def __init__(
        self,
        model: str,
        firmware: int,
        hardware: int,
        mac: str,
        bid: str,
        uuid: str,
        profile_id: int,
        owner: str,
        tenant_id: str,
        loop: asyncio.AbstractEventLoop,
        path: str | None = None,
        connection: DigoConnection | None = None,
        super_id: str | None = None,
        mqtt_host: str | None = None,
        mqtt_port: int | None = None,
        http_host: str | None = None,
        http_port: int | None = None,
        token: str | None = None,
    ) -> None:
        """Initialize the DigoDevice class.

        Args:
            model (str): The model of the device.
            firmware (int): The firmware version of the device.
            hardware (int): The hardware version of the device.
            mac (str): The MAC address of the device.
            mqtt_host (str): The MQTT host for communication.
            mqtt_port (int): The MQTT port for communication.
            http_host (str): The HTTP host for communication.
            http_port (int): The HTTP port for communication.
            bid (str): The box ID of the device.
            uuid (str): The UUID of the device.
            profile_id (int): The profile ID of the device.
            owner (str): The owner of the device.
            tenant_id (str): The tenant ID of the device.
            super_id (str, optional): The ID of the super device. Defaults to None.
            token (str): The token for authentication.
            path (str): The path to the storage file.
            connection (DigoConnection | None, optional): The connection object for communication. Defaults to None.
            loop (asyncio.AbstractEventLoop | None, optional): The event loop for asynchronous operations. Defaults to None.

        """
        if not bid:
            if not mac:
                raise ValueError("MAC or BID must be provided")
            bid = mac.replace(":", "")
        
        if not path:
            path = os.path.dirname(os.path.abspath(sys.argv[0]))

        self._loop = loop
        self._uuid = uuid
        self._bid = bid
        self._mac = mac
        self._model = model
        self._firmware: int = int(firmware)
        self._hardware: int = int(hardware)
        self._profile_id = profile_id
        self._owner = owner
        self._tenant_id = tenant_id
        self._super_id = super_id
        if token and connection is None:
            self.connection = DigoConnection(
                loop,
                mqtt_host,
                mqtt_port,
                http_host,
                http_port,
                tenant_id,
                f"DEV#$#{uuid}#$#{bid}",
                token,
            )
        else:
            self.connection = connection
        self._services: list[str] = []
        self._settings: list[str] = []
        self._schedule = DigoSchedule()
        self._store = DigoStore(f"{path}/{bid}.json", loop)
        self._timers: list[DigoAttribute] = []
        self._common_services: list[DigoAttribute] = []
        self._setting_services: list[DigoAttribute] = []
        self._timer: asyncio.TimerHandle | None = None
        self._unsub: Callable | None = None
        self._setup: DigoDeviceSetup = None
        self._tick: int = 0
        self._tc: str = None

    async def start(self):
        """Start the device."""
        if self.connection is None:
            self._store.load()
            if self._store.data:
                self._create_connection()
        if self.connection is None:
            self._setup = DigoDeviceSetup(self)
            await self._setup.start_server()
        elif self._super_id is None:
            if self.connection.connected is False:
                await self.connection.async_start()
            self._unsub = await self.connection.async_subscribe(
                self.get_command_topic(), self._mqtt_handle_message
            )
            self.connection.connect_signal(MQTT_CONNECTED, self._on_mqtt_connect)
        if self._timer:
            self._timer.cancel()
        self._timer = self._loop.call_later(1, self._loop_handle)

    async def stop(self):
        """Stop the device."""
        if self._setup:
            await self._setup.stop_server()
        elif self._super_id is None and self.connection is not None:
            if self._unsub:
                self._unsub()
                self._unsub = None
            if self.connection.connected is True:
                await self.connection.async_stop()
            self.connection.disconnect_signal(MQTT_CONNECTED, self._on_mqtt_connect)
        if self._timer:
            self._timer.cancel()
            self._timer = None

    def setup_data(self, data: dict):
        """Set up the data."""
        self._store.data["setup"] = data
        self._loop.create_task(self.async_setup_data())

    async def async_setup_data(self):
        """Set up the data asynchronously."""
        await self._store.async_save()
        self._create_connection()
        if self.connection is not None:
            await self.connection.async_start()
            self._unsub = await self.connection.async_subscribe(
                self.get_command_topic(), self._mqtt_handle_message
            )
            self.connection.connect_signal(MQTT_CONNECTED, self._on_mqtt_connect)

    @property
    def bid(self) -> str:
        """Return the box ID."""
        return self._bid

    @property
    def uuid(self) -> str:
        """Return the UUID."""
        return self._uuid

    @property
    def model(self) -> str:
        """Return the model."""
        return self._model

    @property
    def firmware(self) -> int:
        """Return the firmware version."""
        return self._firmware

    @property
    def hardware(self) -> int:
        """Return the hardware version."""
        return self._hardware

    @property
    def mac(self) -> str:
        """Return the MAC address."""
        return self._mac

    @property
    def is_sub(self) -> bool:
        """Return if the device is a sub device."""
        return bool(self._super_id)

    def get_state_topic(self):
        """Get the state topic."""
        if self._super_id:
            return f"v1/iot/devices/{self._super_id}/response"
        return f"v1/iot/devices/{self._uuid}/response"

    def get_command_topic(self):
        """Get the command topic."""
        if self._super_id:
            return f"v1/iot/devices/{self._super_id}/request"
        return f"v1/iot/devices/{self._uuid}/request"

    def get_upload_path(self):
        """Get the upload path."""
        return "/gw/device-common-service/api/v1/device/upload"

    def get_notify_path(self):
        """Get the notify path."""
        return "/gw/device-common-service/api/v1/device/notify"

    def get_update_path(self):
        """Get the update path."""
        return "/gw/ota-service/api/v1/device/ota/check-update"

    def add_common_service(self, attribute: DigoAttribute):
        """Add attributes."""
        if attribute.vtype == dict:
            for child in attribute.children:
                child.update_callback = self._update_common_service
                self._common_services.append(child)
        self._common_services.append(attribute)
        attribute.update_callback = self._update_common_service

    def add_setting_service(self, attribute: DigoAttribute):
        """Add setting attributes."""
        if attribute.vtype == dict:
            raise ValueError("Setting attribute cannot be dict type.")
        attribute.update_callback = self._update_setting_service
        self._setting_services.append(attribute)

    def add_timer(self, name: str):
        """Add timer attributes."""
        attribute = DigoAttribute(name, DigoAttribute.TYPE_INETGER, 0, None)
        self._timers.append(attribute)
        self.add_common_service(attribute)

    def restore_schedule(self):
        """Restore the schedule."""
        if "schedules" in self._store.data:
            self._schedule.add(self._store.data["schedules"])

    def save_schedule(self, delay: int = 30):
        """Save the schedule."""
        self._store.data["schedules"] = self._schedule.get()
        self._store.async_save_delay(delay)

    def _get_common_service(self, key: str) -> DigoAttribute | None:
        """Get the attributes."""
        for attr in self._common_services:
            if attr.key == key:
                return attr
        return None

    def _get_setting_service(self, key: str) -> DigoAttribute | None:
        """Get the attributes."""
        for attr in self._setting_services:
            if attr.key == key:
                return attr
        return None

    def _update_common_service(self, value: dict) -> None:
        """Update the attributes."""
        self._loop.create_task(
            self._mqtt_publish("command", generate_transcode(), value)
        )

    def _update_setting_service(self, value: dict) -> None:
        """Update the setting attributes."""
        self._loop.create_task(
            self._mqtt_publish("command", generate_transcode(), {"settings": value})
        )

    def mqtt_publish(
        self, method: str, tc: str | None, value: dict | None = None
    ) -> None:
        """Publish data to MQTT."""
        if tc is None:
            tc = generate_transcode()
        self._loop.create_task(self._mqtt_publish(method, tc, value))

    def mqtt_publish_with_bid(
        self, bid: str, method: str, tc: str, value: dict | None = None
    ) -> None:
        """Publish data to MQTT with box ID."""
        info = {
            "tenant_id": self._tenant_id,
            "device_id": self._uuid,
            "device_profile_id": self._profile_id,
            "device_owner": self._owner,
            "box_id": bid,
        }
        self._loop.create_task(self._mqtt_publish(method, tc, value, True, info))

    def push_notify(self, title: str, code: str, value: str) -> None:
        """Push notification to Digo."""
        values = {
            "title_code": title,
            "message_code": code,
            "value": value,
        }
        self._loop.create_task(
            self.http_post(self.get_notify_path(), None, "notify", values)
        )

    def reboot_device(self):
        """Reboot the device."""
        raise NotImplementedError

    def reset_device(self):
        """Reset the device."""
        if self.connection is not None and self.connection.connected:
            self.mqtt_publish("reset", self._tc)
        self._loop.create_task(self._async_reset_device())
        
    async def _async_reset_device(self):
        """Reboot the device asynchronously."""
        if "setup" in self._store.data:
            del self._store.data["setup"]
            await self._store.async_save()
        await self.stop()
        self.connection = None
        await self.start()

    @callback
    def async_mqtt_handle_message(self, jmsg: dict) -> None:
        """Handle the message."""
        if "method" in jmsg and "tc" in jmsg:
            method = jmsg["method"]
            self._tc = jmsg["tc"]
            user = self._tc.split("$")[0]

            if method == "get_status":
                self._loop.create_task(self._report_status())
            elif method == "get_extend_status":
                self._loop.create_task(self._report_ex_status())
            elif method == "command" and "values" in jmsg:
                if "settings" in jmsg["values"]:
                    self._setting_command(jmsg["values"]["settings"], user)
                else:
                    self._service_command(jmsg["values"], user)
            elif method == "get_schedule":
                self._schedule_get()
            elif method == "add_schedule":
                self._schedule_add(jmsg)
            elif method == "edit_schedule":
                self._schedule_edit(jmsg)
            elif method == "del_schedule":
                self._schedule_del(jmsg)
            elif method == "reboot":
                self.reboot_device()
            elif method == "reset":
                self.reset_device()
            else:
                logger.debug("Unknown method: %s", method)

    def _service_command(self, services: dict, user: str):
        """Handle service command."""
        keys = list(services.keys())
        self._loop.create_task(
            self._mqtt_publish("command", self._tc, services.copy())
        )
        for key in keys:
            if not services:
                break
            if (value := services[key]) is not None:
                if attr := self._get_common_service(key):
                    if attr.parent:
                        attr.import_value(services, user)
                    else:
                        attr.import_value(value, user)

    def _setting_command(self, settings: dict, user: str):
        """Handle setting command."""
        for key, value in settings.items():
            if attr := self._get_setting_service(key):
                attr.import_value(value, user)

    @callback
    def _on_mqtt_connect(self):
        """Handle MQTT connect."""
        if self._setup:
            self._loop.create_task(self._setup.stop_server())
        self._loop.create_task(self._report_status())
        self._loop.create_task(self._report_ex_status())

    @callback
    def _loop_handle(self):
        """Handle loop."""
        self._timer = self._loop.call_at(self._loop.time() + 1.0, self._loop_handle)

        for timer in self._timers:
            if timer.value > 0:
                timer.value -= 1
                if (timer.value >= 60 and timer.value % 60 == 0) or (
                    timer.value < 30 and timer.value % 10 == 0
                ):
                    timer.set_value(timer.value)
                if (
                    timer.value == 0
                    and (attr := self._find_service_match_timer(timer.key))
                    and attr.vtype == bool
                ):
                    attr.import_value(not attr.value, "timer")

        self._tick += 1
        if self._tick >= 30:
            for item in self._schedule.process():
                if target := self._schedule.target(item):
                    self._service_command(target, "schedule")
                if self._schedule.check_notify(item):
                    self.push_notify("TITLE_DI", "SCHDED_01", 0)
            if self._schedule.check_save():
                self.save_schedule(10)
            self._tick = 0

    def _mqtt_handle_message(self, msg: ReceiveMessage) -> None:
        """Handle MQTT message."""
        try:
            jmsg = json.loads(msg.payload)
        except json.JSONDecodeError:
            logger.info("Mqtt payload is not valid JSON")
        else:
            self._loop.call_soon_threadsafe(self.async_mqtt_handle_message, jmsg)

    async def _report_status(self):
        """Get the status."""
        values: dict = {}
        if not self._tc:
            self._tc = generate_transcode()
        for attr in self._common_services:
            if value := attr.export_value():
                values.update(value)
        await self._mqtt_publish("status", self._tc, values)

    def _get_ssid(self) -> str:
        """Get the SSID.

        Overridden by DigoDevice types.
        """
        return ""

    def _get_ip(self) -> str:
        """Get the IP address.

        Overridden by DigoDevice types.
        """
        return ""

    def _get_signal(self) -> int:
        """Get the signal.

        Overridden by DigoDevice types.
        """
        return -1

    async def _report_ex_status(self):
        """Get the extended status."""
        value = {
            "fw": self._firmware,
            "hw": self._hardware,
            "pid": f"{self._model}.{self._bid}",
            "signal": self._get_signal(),
            "rst": None,
            "ip": self._get_ip(),
            "ssid": self._get_ssid(),
            "mac": self._mac,
        }

        settings: dict = {}
        if not self._tc:
            self._tc = generate_transcode()
        for attr in self._setting_services:
            settings.update(attr.export_value())
        if settings:
            value["settings"] = settings

        task1 = self._mqtt_publish("extend_status", self._tc, value)
        task2 = self.http_post(self.get_upload_path(), self._tc, "extend_status", value)
        await asyncio.gather(task1, task2)

    def _info(self) -> dict:
        """Get the information about the device."""
        return {
            "tenant_id": self._tenant_id,
            "device_id": self._uuid,
            "device_profile_id": self._profile_id,
            "device_owner": self._owner,
            "box_id": self._bid,
        }

    async def _mqtt_publish(
        self,
        method: str,
        tc: str,
        value: dict | None = None,
        success: bool = True,
        info: dict | None = None,
    ) -> None:
        """Publish data to MQTT."""
        if not info:
            info = self._info()
        data = {
            "ts": get_timestamp(),
            "tc": tc,
            "info": info,
            "method": method,
            "success": success,
        }
        if value is not None:
            data["values"] = value
        if self.connection and self.connection.connected:
            await self.connection.async_publish(
                self.get_state_topic(), json.dumps(data)
            )
        else:
            logger.error("Device %s is not connected to MQTT", self._uuid)

    async def http_post(
        self, path: str, tc: str, method: str, value: dict | None
    ) -> dict:
        """Post data to Digo."""
        if not tc:
            tc = generate_transcode()
        data = {
            "ts": get_timestamp(),
            "tc": tc,
            "info": self._info(),
            "method": method,
            "success": True,
        }
        if value is not None:
            data["values"] = value
        if self.connection:
            return await self._loop.run_in_executor(
                None, self.connection.post, path, data
            )
        return {}

    def _create_connection(self):
        """Create the connection."""
        sdata: dict[str, Any] = self._store.data.get("setup", {})
        info: dict = sdata.get("info") if sdata else None
        logger.debug("Device setup data: %s", sdata)
        if info:
            mqtt_host = sdata.get("host")
            mqtt_port = sdata.get("port")
            http_host = sdata.get("host_http")
            http_port = sdata.get("port_http")
            token = info.get("token")
            self._tenant_id = info.get("tenant_id")
            self._uuid = info.get("device_id")
            self._profile_id = info.get("device_profile_id")
            self._owner = info.get("owner")
            self.connection = DigoConnection(
                self._loop,
                mqtt_host,
                mqtt_port,
                http_host,
                http_port,
                self._tenant_id,
                f"DEV#$#{self.uuid}#$#{self._bid}",
                token,
            )
        else:
            logger.error("Device setup data missing")

    def _find_service_match_timer(self, key: str) -> DigoAttribute | None:
        """Find the service match timer."""
        service = None
        if key == "timer":
            service = "switch"
        elif key.startswith("timer_"):
            extra = key[6:]
            if extra in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                service = f"switch_{extra}"
            else:
                service = extra
        if service:
            for serv in self._common_services:
                if serv.key == service:
                    return serv
        return None

    def _schedule_get(self) -> None:
        """Get the schedule."""
        schedule = {"schedules": self._schedule.get()}
        self._loop.create_task(self._mqtt_publish("get_schedule", self._tc, schedule))

    def _schedule_add(self, values: dict) -> None:
        """Add the schedule."""
        status = False
        if "values" in values and (schedules := values["values"]["schedules"]):
            status = self._schedule.add(schedules)
            if status:
                self.save_schedule()
        self._loop.create_task(
            self._mqtt_publish("add_schedule", self._tc, None, status)
        )

    def _schedule_edit(self, values: dict) -> None:
        """Edit the schedule."""
        if "values" in values and (schedules := values["values"]["schedules"]):
            if self._schedule.edit(schedules):
                self.save_schedule()
        self._loop.create_task(self._mqtt_publish("edit_schedule", self._tc))

    def _schedule_del(self, values: dict) -> None:
        """Delete the schedule."""
        if "values" in values and (schedules := values["values"]["schedules"]):
            if self._schedule.remove(schedules):
                self.save_schedule()
        self._loop.create_task(self._mqtt_publish("del_schedule", self._tc))
