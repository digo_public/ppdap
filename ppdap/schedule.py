"""Support for DigoBridge schedule."""

import logging

from .utils import get_timenow

logger = logging.getLogger(__name__)


class DigoSchedule:
    """Class representing a schedule."""

    def __init__(self) -> None:
        """Initialize an empty schedule."""
        self._schedules: dict[str, dict] = {}
        self._execute_time = None
        self._save = False

    def get(self) -> list[dict]:
        """Return a list of all schedules."""
        return list(self._schedules.values())

    def add(self, schedules: list[dict]) -> bool:
        """Add a list of schedules to the existing schedules.

        Each schedule is a dictionary with keys: 'code', 'enable', 'time', 'repeat', 'notify', 'target'.
        """
        status = False
        for item in schedules:
            if (
                "code" not in item
                or "enable" not in item
                or "time" not in item
                or "repeat" not in item
                or "notify" not in item
                or "target" not in item
            ):
                continue
            status = True
            self._schedules[item["code"]] = item
        return status

    def edit(self, schedules: list[dict]) -> bool:
        """Edit existing schedules with the provided list of schedules.

        Each schedule is a dictionary with keys: 'code', 'enable', 'time', 'repeat', 'notify', 'target'.
        """
        status = False
        for item in schedules:
            if "code" not in item:
                continue
            if item["code"] not in self._schedules:
                continue
            if "enable" in item:
                self._schedules[item["code"]]["enable"] = item["enable"]
            if "time" in item:
                self._schedules[item["code"]]["time"] = item["time"]
            if "repeat" in item:
                self._schedules[item["code"]]["repeat"] = item["repeat"]
            if "notify" in item:
                self._schedules[item["code"]]["notify"] = item["notify"]
            if "target" in item:
                self._schedules[item["code"]]["target"] = item["target"]
            status = True
        return status

    def remove(self, schedules: list[dict]) -> bool:
        """Delete the provided list of schedules from the existing schedules.

        Each schedule is a dictionary with a key: 'code'.
        """
        status = False
        for item in schedules:
            if "code" not in item:
                continue
            if item["code"] in self._schedules:
                del self._schedules[item["code"]]
                status = True
        return status

    def target(self, schedule: dict) -> dict:
        """Return the target of the schedule."""
        if isinstance(schedule["target"], dict):
            return schedule["target"].copy()
        return {}

    def check_notify(self, schedule: dict) -> bool:
        """Check if the schedule with the provided code should notify."""
        if "notify" in schedule and schedule["notify"] is True:
            return True
        return False

    def check_save(self) -> bool:
        """Check if there are any schedules that need to be saved."""
        save = self._save
        self._save = False
        return save

    def process(self) -> list[dict]:
        """Return a list of schedules that are enabled, match the current time, and either repeat or are one-time."""
        ret: list[dict] = []
        if not self._schedules:
            return ret

        t = get_timenow()
        current_time = t.strftime("%H%M")

        for item in self._schedules.values():
            if (
                item["enable"] is True
                and (item["repeat"] == 0 or (item["repeat"] & (1 << t.weekday())) != 0)
                and item["time"] == current_time
            ):
                ret.append(item)
                if item["repeat"] == 0:
                    item["enable"] = False
                    self._save = True
        if ret:
            if self._execute_time == current_time:
                ret.clear()
            self._execute_time = current_time
        else:
            self._execute_time = None
        return ret
