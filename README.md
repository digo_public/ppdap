# ppdap


This project enables communication with the DIGO platform using Python.

Requirements
------------

* python 3.11+
* python pip 22+
* paho-mqtt (1.6.1)
* aiohttp
* requests
* atomicwrites
* datetime


Installation
------------

    $ pip install ppdap



Usage
-----

Check [examples](https://git.digotech.net/digo_public/example_pydap.git)