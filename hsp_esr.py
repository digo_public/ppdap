import asyncio
import logging
import zoneinfo
from ppdap.device import DigoDevice
from ppdap.attribute import DigoAttribute
from ppdap.utils import set_default_time_zone

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("main")

async def update_data(esr: DigoAttribute, state: DigoAttribute, battery: DigoAttribute, heart_rate: DigoAttribute):
    """Update the data."""
    tmp = 0
    while True:
        await asyncio.sleep(10)
        tmp += 1
        if tmp > 100:
            tmp = 0
        esr.set_value(tmp)
        state.set_value(True if tmp % 2 == 0 else False)
        battery.set_value(100 - tmp)
        heart_rate.set_value(60 + tmp)

if __name__ == '__main__':
    """Main function."""

    set_default_time_zone(zoneinfo.ZoneInfo("Asia/Ho_Chi_Minh"))
    loop = asyncio.get_event_loop()
    
    device = DigoDevice("HSP_ESR", 100, 101, "00:11:22:33:44:55", "", "", 0, "", "", loop)
    
    esr = DigoAttribute("esr_1", DigoAttribute.TYPE_INETGER, 0, None)
    device.add_common_service(esr)
    state = DigoAttribute("state_1", DigoAttribute.TYPE_BOOLEAN, 0, None)
    device.add_common_service(state)
    battery = DigoAttribute("battery_1", DigoAttribute.TYPE_INETGER, 0, None)
    device.add_common_service(battery)
    heart_rate = DigoAttribute("heart_rate_1", DigoAttribute.TYPE_INETGER, 0, None)
    device.add_common_service(heart_rate)
    node_addr = DigoAttribute("node_addr_1", DigoAttribute.TYPE_STRING, device.mac, None)
    device.add_common_service(node_addr)
    mac_addr = DigoAttribute("mac_addr_1", DigoAttribute.TYPE_STRING, "", None)
    device.add_common_service(mac_addr)
    loop.run_until_complete(device.start())
    loop.create_task(update_data(esr, state, battery, heart_rate))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(device.stop())