import asyncio
import logging
import zoneinfo
from typing import Any
from ppdap.device import DigoDevice
from ppdap.attribute import DigoAttribute
from ppdap.utils import set_default_time_zone


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("main")

def on_state_change(key: str, value: Any, user: Any):
    """Callback function for state change."""
    logger.info("State %s changed to %s", key, value)

if __name__ == '__main__':
    """Main function."""

    set_default_time_zone(zoneinfo.ZoneInfo("Asia/Ho_Chi_Minh"))

    loop = asyncio.get_event_loop()
    device = DigoDevice("CT1WF", 100, 101, "00:11:22:33:44:66", "", "", 0, "", "", loop)
    state = DigoAttribute("switch_1", DigoAttribute.TYPE_BOOLEAN, 0, on_state_change)
    device.add_common_service(state)
    device.add_timer("timer_1")
    loop.run_until_complete(device.start())
    device.restore_schedule()

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    finally:
        loop.run_until_complete(device.stop())